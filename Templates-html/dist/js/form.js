$("#client-form").validate({
    errorClass: "is-invalid fail-alert",
    validClass: "is-valid success-alert",
    rules: {
        name: {
            required: true,
            minlength: 3
        },
        lastName: {
            required: true,
            minlength: 3
        },
        tel1: {
            required: true,
            number: true,
            min: 8
        },
        tel2: {
            required: false,
            number: true,
            min: 8
        },
        cin: {
            required: true,
            number: true,
            min: 8
        },
        compte:{
            required: true,
        }
    },
    messages: {
        name: {
            required: "S'il vous plaît entrez votre nom",
            minlength: "Le nom doit comporter au moins 3 caractères "
        },
        lastName: {
            required: "S'il vous plaît entrez votre pénom",
            minlength: "Le prénom doit comporter au moins 3 caractères "
        },
        tel1:  {
            required: "S'il vous plaît entrez votre numéro de téléphone",
            number: "veuillez entrer un numéro de téléphone valide",
            min: "veuillez entrer un numéro de téléphone valide"
        },
        tel2:  {
            number: "veuillez entrer un numéro de téléphone valide",
            min: "veuillez entrer un numéro de téléphone valide"
        },
        cin:  {
            required: "S'il vous plaît entrez votre numéro de CIN",
            number: "veuillez entrer un numéro de CIN valide",
            min: "veuillez entrer un numéro de CIN valide"
        },
        compte: {
            required: "S'il vous plaît choisisser un compte",
        }
    }
});




$("#compte-form").validate({
    errorClass: "is-invalid fail-alert",
    validClass: "is-valid success-alert",
    rules: {
        credit: {
            required: true,
            number: true,
        },
        payment: {
            required: true,
        },
        validate: {
            required: true,

        },
    },
    messages: {
        credit: {
            required: "S'il vous plaît entrez un crédit",
            number: "veuillez entrer un crédit valide "
        },
        payment: {
            required: "S'il vous plaît choisisser un mode de paiement"
        },
        validate:  {
            required: "S'il vous plaît entrez la date d'expiration du compte",
        },
    }
});

$("#hotel-form").validate({
    errorClass: "is-invalid fail-alert",
    validClass: "is-valid success-alert",
    rules: {
        name: {
            required: true,
            minlength: 3
        },
        address: {
            required: true,
            minlength: 3
        },
        directeur: {
            required: true,
            minlength: 3
        },
        userName: {
            required: false,
            minlength: 3
        },
        register: {
            required: true,
            minlength: 8
        },
        Matricule:{
            required: true,
            minlength: 8
        },     
        pass:{
            required: true,
            minlength: 4
        },        
        pass1:{
            required: true,
            minlength: 4,
            equalTo: "#pass"
        }
    },
    messages: {
        name: {
            required: "Ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },
        address: {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },
        directeur:  {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },
        userName:  {
            number: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },
        register:  {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 8 caractères"
        },
        Matricule: {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 8 caractères"
        },        
        pass:  {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 4 caractères"
        },
        pass1: {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 4 caractères"
        }
    }
});




$("#menu-form").validate({
    errorClass: "is-invalid fail-alert",
    validClass: "is-valid success-alert",
    rules: {
        name: {
            required: true,
            minlength: 3
        },
        prix: {
            required: true,
            number: true
        },
        zone: {
            required: true,
        }
        
    },
    messages: {
        name: {
            required: "Ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },
        prix: {
            required: "ce champ est obligatoire",
            number: "veuillez entrer un prix valide "
        },
        zone:  {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        }
    }
});


$("#serveur-form").validate({
    errorClass: "is-invalid fail-alert",
    validClass: "is-valid success-alert",
    rules: {
        name: {
            required: true,
            minlength: 3
        },
        userName: {
            required: true,
            minlength: 3
        },
        zone: {
            required: true,
            
        },
        pass:{
            required: true,
            minlength: 4
        },        
        pass1:{
            required: true,
            minlength: 4,
            equalTo: "#pass"
        }
    },
    messages: {
        name: {
            required: "Ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },
        userName: {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },
        zone:  {
            required: "ce champ est obligatoire",
        }, 
        pass:  {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 4 caractères"
        },
        pass1: {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 4 caractères"
        }
    }
});


$("#subadmin-form").validate({
    errorClass: "is-invalid fail-alert",
    validClass: "is-valid success-alert",
    rules: {
        name: {
            required: true,
            minlength: 3
        },
        userName: {
            required: false,
            minlength: 3
        },     
        pass:{
            required: true,
            minlength: 4
        },        
        pass1:{
            required: true,
            minlength: 4,
            equalTo: "#pass"
        }
    },
    messages: {
        name: {
            required: "Ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },
        userName:  {
            number: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        },       
        pass:  {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 4 caractères"
        },
        pass1: {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 4 caractères"
        }
    }
});


$("#zone-form").validate({
    errorClass: "is-invalid fail-alert",
    validClass: "is-valid success-alert",
    rules: {
        name: {
            required: true,
            minlength: 3
        }
    },
    messages: {
        name: {
            required: "Ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 3 caractères "
        }
    }
});


$("#admin-form").validate({
    errorClass: "is-invalid fail-alert",
    validClass: "is-valid success-alert",
    rules: {
        pass:{
            required: true,
            minlength: 4
        },        
        pass1:{
            required: true,
            minlength: 4,
            equalTo: "#pass"
        }
    },
    messages: {
        pass:  {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 4 caractères"
        },
        pass1: {
            required: "ce champ est obligatoire",
            minlength: "Ce champ doit comporter au moins 4 caractères"
        }
    }
});